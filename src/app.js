const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');

const appHooks = require('./app.hooks');
const channels = require('./channels');
const mongodb = require('./mongodb');

const receivers = require('./infraestructure/receivers');
const adapters = require('./infraestructure/adapters');
const controllers = require('./domain/controllers');

const app = express(feathers());

app.configure(configuration());
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
app.use('/', express.static(app.get('public')));

app.configure(express.rest());
app.configure(socketio());
app.configure(mongodb);
app.configure(adapters);
app.configure(controllers);
app.configure(receivers);
app.configure(channels);

app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;
