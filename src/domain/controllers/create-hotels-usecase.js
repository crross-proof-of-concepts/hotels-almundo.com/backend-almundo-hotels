const Hotel = require('../models/hotel');

class CreateHotelsuseCase {
  constructor(repository) {
    this.repository = repository;
  }

  createHotel(hotelData) {
    let hotelInstance = this.hotelFactory(hotelData);
    this.repository.save(hotelInstance);
    return hotelInstance;
  }

  hotelFactory(hotelData) {
    let { name, price, stars, image, amenities } = hotelData;

    if (!name) return null;
    if (!price) return null;
    if (!stars) stars = 0;
    if (!image) image = '';
    if (!amenities || !Array.isArray(amenities)) amenities = [];

    return new Hotel(name, stars, price, image, amenities);
  }
}

module.exports = CreateHotelsuseCase;
