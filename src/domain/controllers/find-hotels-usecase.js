class FindHotelsuseCase {
  constructor(repository) {
    this.repository = repository;
  }

  findHotel(params) {
    return this.repository.find(params);
  }
}

module.exports = FindHotelsuseCase;
