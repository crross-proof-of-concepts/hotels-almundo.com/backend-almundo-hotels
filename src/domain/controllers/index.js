const CreateHotelsUseCase = require('./create-hotels-usecase');
const FindHotelsUseCase = require('./find-hotels-usecase');

module.exports = function(app) {
  injectUseCase(app);
};

function injectUseCase(app) {
  const repository = app.get('hotelRepository');
  app.set('createHotelsUseCase', new CreateHotelsUseCase(repository));
  app.set('findHotelsUseCase', new FindHotelsUseCase(repository));
}
