class Hotel {
  constructor(name, stars, price, image, amenities) {
    this.name = name;
    this.stars = stars;
    this.price = price;
    this.image = image;
    this.amenities = amenities;
  }
}

module.exports = Hotel;
