module.exports = {
  before: {
    find: [
      context => {
        let { stars, priceMin, priceMax } = context.params.query;
        context.params.query.stars = parseToNumber(stars);
        context.params.query.priceMin = parseToNumber(priceMin);
        context.params.query.priceMax = parseToNumber(priceMax);
        return context;
      }
    ]
  }
};

function parseToNumber(value) {
  if (Array.isArray(value)) {
    return value.map(v => parseToNumber(v));
  }

  if (!isNaN(value)) {
    return parseInt(value);
  }

  return null;
}
