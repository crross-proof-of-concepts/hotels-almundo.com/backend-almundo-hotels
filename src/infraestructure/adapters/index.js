const HotelAdapter = require('./mongodb-repository/hotels-repository-adapter');

module.exports = function(app) {
  injectHotelsAdapter(app);
};

function injectHotelsAdapter(app) {
  const mongoClient = app.get('mongoClient');
  app.set('hotelRepository', new HotelAdapter(mongoClient));
}
