class HotelsRepositoryAdapter {
  constructor(mongoClient) {
    this.mongoClient = mongoClient;
    this.collection;
  }

  async save(data) {
    let response = await this.mongoClient.then(async db => {
      return await db.collection('hotels').insertOne(data, null);
    });
    return response.result;
  }

  async find(params) {
    let response = await this.mongoClient.then(async db => {
      return await db
        .collection('hotels')
        .find(this.buildQueryParam(params))
        .toArray();
    });
    return response;
  }

  buildQueryParam(params) {
    let query = {};
    if (params.search) {
      query.name = new RegExp(params.search, 'i');
    }
    if (params.amenities)
      query.amenities = this.filterAmenities(params.amenities);
    if (params.stars) query.stars = this.filterStars(params.stars);
    if (params.priceMin || params.priceMax)
      query.price = this.filterPriceRange(params.priceMin, params.priceMax);

    return query;
  }

  filterAmenities(amenities) {
    let query = {};
    if (amenities && Array.isArray(amenities)) {
      query = { $in: amenities };
    } else if (amenities) {
      query = amenities;
    }
    return query;
  }

  filterStars(stars) {
    let query = {};
    if (stars && Array.isArray(stars)) {
      query = { $in: stars };
    } else if (stars) {
      query = stars;
    }
    return query;
  }

  filterPriceRange(priceMin, priceMax) {
    let query = {};
    if (priceMin && Array.isArray(priceMin)) {
      query = { $gte: priceMin[0] };
    } else if (priceMin) {
      query = { $gte: priceMin };
    }

    if (priceMax && Array.isArray(priceMax)) {
      query = { $lte: priceMax[0] };
    } else if (priceMax) {
      query = Object.assign(query, { $lte: priceMax });
    }

    return query;
  }
}

module.exports = HotelsRepositoryAdapter;
