const HotelsService = require('./web-rest/hotels-service-receiver');

const hotelsHooks = require('../../hooks/hotels.hooks');

module.exports = function(app) {
  app.configure(injectHotelsService);
};

function injectHotelsService(app) {
  const options = {
    createUseCase: app.get('createHotelsUseCase'),
    queryUseCase: app.get('findHotelsUseCase')
  };

  app.use('/hotels', HotelsService(options));
  const service = app.service('hotels');
  service.hooks(hotelsHooks);
}
