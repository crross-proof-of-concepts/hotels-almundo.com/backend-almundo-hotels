class Service {
  constructor(options) {
    this.options = options || {};
  }

  async find(params) {
    return this.options.queryUseCase.findHotel(params.query);
  }

  async create(data) {
    if (Array.isArray(data)) {
      return await data.map(async d => await this.create(d));
    }

    return await this.options.createUseCase.createHotel(data);
  }
}

module.exports = function(options) {
  return new Service(options);
};

module.exports.Service = Service;
